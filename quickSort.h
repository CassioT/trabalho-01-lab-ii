void swap(int*, int*);

int partition(int [], int, int);

void quickSort(int [], int, int, int*);

void quickSortSemCauda(int [], int, int, int*);

void quickSortSemCaudaMenor(int [], int, int, int*);

int selectIdx(int array[], int lo, int hi);

int median(int array[], int lo, int hi, int med);

int partitionMedian(int array[], int lo, int hi, int pivotIndex);

void quickSortMedian(int array[ ], int lo, int hi, int*);

void quickSortTresPart(int [], int, int, int*);

void quickSortON2(int array[], int lo, int hi, int* contador);

int partitionPointer(int* pointer, int* hi);

void quickSortPointer(int* pointer, int* hi, int* contador);
