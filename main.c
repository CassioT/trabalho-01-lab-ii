#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "quickSort.h"

int main(int argc, char* argv[])
{
	int N;
	int M;
	int v;

	printf("Digite a quantidade de elementos do vetor: ");
	scanf("%d", &v);
	__fpurge(stdin);
	printf("Digite o primeiro numero do intervalo: ");
	scanf("%d", &M);
	__fpurge(stdin);
	printf("Digite o segundo numero do intervalo: ");
	scanf("%d", &N);
	__fpurge(stdin);
	int array[v];
	int* pointer = NULL;
	int *endPointer = NULL;
	pointer = (int*)malloc(sizeof(int)*v);
	int i, aux;
	double msec;
	double start, diff;
	int contador = 0;
	srand(time(NULL));

	for (i = 0; i < v; ++i){
		array[i] = M + rand() / (RAND_MAX / (N - M + 1) + 1);
	}

	memcpy(pointer, array, sizeof(array));
	endPointer = pointer+(v-1);

	start = clock();
	quickSort(pointer, 0, v-1, &contador);
	diff = clock() - start;
	msec = diff / CLOCKS_PER_SEC;

	printf("################################\n");
	printf("--------------------------------\n");
	printf("qSort1 levou %lf segundos\n", msec);
	printf("qSort1: %d foi o tamanho maximo da pilha.\n", contador);
	printf("--------------------------------\n");

	for (i = 0; i < v-1; ++i)
	{
		if(pointer[i]>pointer[i+1]){
			printf("ERRADO!!!!!!!!!\n");
		}
	}

	contador = 0;
	memcpy(pointer, array, sizeof(array));

	start = clock();
	quickSortSemCauda(pointer, 0, v-1, &contador);
	diff = clock() - start;
	msec = diff / CLOCKS_PER_SEC;

	printf("qSort2 levou %lf segundos\n", msec);
	printf("qSort2: %d foi o tamanho maximo da pilha.\n", contador);
	printf("--------------------------------\n");

	for (i = 0; i < v-1; ++i)
	{
		if(pointer[i]>pointer[i+1]){
			printf("ERRADO!!!!!!!!!\n");
		}
	}

	contador = 0;
	memcpy(pointer, array, sizeof(array));

	start = clock();
	quickSortSemCaudaMenor(pointer, 0, v-1, &contador);
	diff = clock() - start;
	msec = diff / CLOCKS_PER_SEC;

	printf("qSort3 levou %lf segundos\n", msec);
	printf("qSort3: %d foi o tamanho maximo da pilha.\n", contador);
	printf("--------------------------------\n");

	for (i = 0; i < v-1; ++i)
	{
		if(pointer[i]>pointer[i+1]){
			printf("ERRADO!!!!!!!!!\n");
		}
	}

	contador = 0;
	memcpy(pointer, array, sizeof(array));

	start = clock();
	quickSortMedian(pointer, 0, v-1, &contador);
	diff = clock() - start;
	msec = diff / CLOCKS_PER_SEC;

	printf("qSort4 levou %lf segundos\n", msec);
	printf("qSort4: %d foi o tamanho maximo da pilha.\n", contador);
	printf("--------------------------------\n");

	for (i = 0; i < v-1; ++i)
	{
		if(pointer[i]>pointer[i+1]){
			printf("ERRADO!!!!!!!!!\n");
		}
	}
	

	contador = 0;
	memcpy(pointer, array, sizeof(array));

	start = clock();
	quickSortTresPart(pointer, 0, v-1, &contador);
	diff = clock() - start;
	msec = diff / CLOCKS_PER_SEC;

	printf("qSort5 levou %lf segundos\n", msec);
	printf("qSort5: %d foi o tamanho maximo da pilha.\n", contador);
	printf("--------------------------------\n");

	for (i = 0; i < v-1; ++i)
	{
		if(pointer[i]>pointer[i+1]){
			printf("ERRADO!!!!!!!!!\n");
		}
	}

	contador = 0;
	memcpy(pointer, array, sizeof(array));

	start = clock();
	quickSortON2(pointer, 0, v-1, &contador);
	diff = clock() - start;
	msec = diff / CLOCKS_PER_SEC;

	printf("qSort6 levou %lf segundos\n", msec);
	printf("qSort6: %d foi o tamanho maximo da pilha.\n", contador);
	printf("--------------------------------\n");

	for (i = 0; i < v-1; ++i)
	{
		if(pointer[i]>pointer[i+1]){
			printf("ERRADO!!!!!!!!!\n");
		}
	}

	contador = 0;
	memcpy(pointer, array, sizeof(array));

	start = clock();
	quickSortPointer(pointer, endPointer, &contador);
	diff = clock() - start;
	msec = diff / CLOCKS_PER_SEC;

	printf("qSort7 levou %lf segundos\n", msec);
	printf("qSort7: %d foi o tamanho maximo da pilha.\n", contador);
	printf("--------------------------------\n");

	for (i = 0; i < v-1; ++i)
	{
		if(pointer[i]>pointer[i+1]){
			printf("ERRADO!!!!!!!!!\n");
		}
	}
	printf("################################\n");
		
	return 0;
}

