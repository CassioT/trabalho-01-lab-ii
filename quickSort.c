#include <execinfo.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void swap(int* a, int* b){
	int aux;
	aux = *a;
	*a = *b;
	*b = aux;	
}

int partition(int array[], int lo, int hi){
	int i;
	int j;
	i = lo;
	for(j = lo + 1; j <= hi; ++j){
		if(array[j] < array[lo]){
			++i;
			swap(&array[i], &array[j]);
		}
	}
	swap(&array[lo], &array[i]);
	return i;
}

void quickSort(int array[], int lo, int hi, int* contador){
	int p;
	void* buffer[5000];
	int stack = backtrace(buffer, 5000);
	if(stack > *contador){
		*contador = stack;
	}
	if(lo < hi){
		p = partition(array, lo, hi);
		quickSort(array, lo, p-1, contador);
		quickSort(array, p+1, hi, contador);	
	}
}

void quickSortSemCauda(int array[], int lo, int hi, int* contador){
	int p;
	void* buffer[5000];
	int stack = backtrace(buffer, 5000);
	if(stack > *contador){
		*contador = stack;
	}
	if (lo < hi){
		while (lo < hi){
			p = partition(array, lo, hi);
			quickSortSemCauda(array, lo, p-1, contador);
			lo = p+1;
		}
	}
}

void quickSortSemCaudaMenor(int array[], int lo, int hi, int* contador){
	int p;
	void* buffer[5000];
	int stack = backtrace(buffer, 5000);
	if(stack > *contador){
		*contador = stack;
	}
	if (lo < hi){
		while (lo < hi){
			p = partition(array, lo, hi);
			if (p - lo < hi - p)
			{
				quickSortSemCaudaMenor(array, lo, p-1, contador);
				lo = p+1;
			} else {
				quickSortSemCaudaMenor(array, p+1, hi, contador);
				hi = p-1;
			}
			
		}
	}
}

int selectIdx(int array[], int lo, int hi, int med){
	int i, j, chave;
	for (j = 1; j < hi; ++j){
		chave = array[j];
		i = j-1;
		while(i >= lo && array[i] > chave){
        	array[i + 1] = array[i];
        	i = i-1;
      	}
      	array[i + 1] = chave;
	}
	return med;
}

// returns the index of the median of medians.
// requires a variant of select, "selectIdx"
// which returns the index of the selected item rather than the value
int median(int array[], int lo, int hi){
    int i, subLo, subHi, medianIdx, med;
    int numMedians;
    if(((hi-lo)%5) > 0){
    	numMedians = ((hi-lo)/5) +1;
    } else {
    	numMedians = ((hi-lo)/5);
    }
    if(hi-lo <= 5){
    	if(((hi-lo)%2) != 0){
    		med = ((hi-lo)/2) +1 + lo;
    	} else {
    		med = ((hi-lo)/2) + lo;
    	}
    	medianIdx = selectIdx(array, lo, hi, med);
    	return medianIdx;
    }
    for (i = 0; i < numMedians; ++i){
    	// get the median of the five-element subgroup
    	subLo = lo + (i*5);
    	subHi = subLo + 5;
    	if (subHi > hi) subHi = hi;
    	// alternatively, use a faster method that works on lists of size 5
    	if(((subHi-subLo)%2) != 0){
    		med = ((subHi-subLo)/2) +1;
    	} else {
    		med = ((subHi-subLo)/2);
    	}
    	medianIdx = selectIdx(array, subLo, subHi, med+(subLo));
    	// move the median to a contiguous block at the beginning of the list
    	swap(&array[lo+i], &array[medianIdx]);
    	
    }
    return median(array, lo, lo + numMedians-1);
}


int partitionMedian(int array[], int lo, int hi, int pivotIndex){
	int storeIndex, i;
    swap(&array[pivotIndex], &array[lo]);
    storeIndex = lo;
    for(i = lo + 1; i <= hi; ++i){
    	if(array[i] < array[lo]){
    		++storeIndex;
    		swap(&array[i], &array[storeIndex]);
    	}
    }
    for(i=storeIndex+1; i <= hi; ++i){
    	if (array[i] == array[lo])
    	{
    		++storeIndex;
    		swap(&array[i], &array[storeIndex]);
    	}
    }
    swap(&array[lo], &array[storeIndex]);
    return storeIndex;
}     

void quickSortMedian(int array[], int lo, int hi, int* contador){
    int i, j, p, pivotIndex;
    void* buffer[5000];
	int stack = backtrace(buffer, 5000);
	if(stack > *contador){
		*contador = stack;
	}
    if (lo < hi){   	
    	while (lo < hi){
    		j = median(array, lo, hi);
			p = partitionMedian(array, lo, hi, j);
			if (p - lo < hi - p){
	    		quickSortMedian(array, lo, p-1, contador);
				lo = p+1;
			} else {
				quickSortMedian(array, p+1, hi, contador);
				hi = p-1;
			}
		}   
	}
}

void quickSortTresPart(int array[], int lo, int hi, int* contador){ 
	int i = lo-1, j = hi, p = lo-1, q = hi; int v = array[hi];
	int k;
	void* buffer[5000];
	int stack = backtrace(buffer, 5000);
	if(stack > *contador){
		*contador = stack;
	}
	if (hi <= lo) return;
	for (;;){
		while (array[++i] < v) ;
		while (v < array[--j]) {
			if (j == lo) break;
		}
		if (i >= j) break;
		swap(&array[i], &array[j]);
		if (array[i] == v) { 
			p++; 
			swap(&array[p], &array[i]); 
		}
		if (v == array[j]) { 
			q--; 
			swap(&array[j], &array[q]); 
		}
	}
	swap(&array[i], &array[hi]); 
	j = i-1; 
	i = i+1;
	for (k = lo; k < p; k++, j--){
		swap(&array[k], &array[j]);
	}
	for (k = hi-1; k > q; k--, i++){
		swap(&array[i], &array[k]);
	}
	quickSortTresPart(array, lo, j, contador);
	quickSortTresPart(array, i, hi, contador);
}

void quickSortON2(int array[], int lo, int hi, int* contador){
	int p;
	int i, j, chave;
	void* buffer[5000];
	int stack = backtrace(buffer, 5000);
	if(stack > *contador){
		*contador = stack;

	}
	if (lo < hi){
		if (hi-lo <= 5000){
			for (j = 1; j <= hi; ++j){
				chave = array[j];
				i = j-1;
				while(i >= lo && array[i] > chave){
         			array[i + 1] = array[i];
        			i = i-1;
      			}
      			array[i + 1] = chave;
			}

		} else {
			p = partition(array, lo, hi);
			quickSortON2(array, lo, p-1, contador);
			quickSortON2(array, p+1, hi, contador);

		}
		
	}
}

int* partitionPointer(int* pointer, int* hi){
	int *i = pointer;
	int *j = pointer+1;
	for( ; j <= hi; ++j){
		if(*j < *pointer){
			++i;
			swap(i, j);
		}
	}
	swap(pointer, i);
	return i;
}

void quickSortPointer(int* pointer, int* hi, int* contador){
	int *p;
	void* buffer[5000];
	int stack = backtrace(buffer, 5000);
	if(stack > *contador){
		*contador = stack;
	}
	if(pointer < hi){
		while (pointer < hi){
			p = partitionPointer(pointer, hi);
			quickSortPointer(pointer, p-1, contador);
			pointer = p+1;
		}	
	}
}